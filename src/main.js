import { createApp } from 'vue'
import TaskManager from './TaskManager.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

createApp(TaskManager).mount('#app');
