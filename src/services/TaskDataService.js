import http from '../http-common'

class TaskDataService {
    getAll() {
        return http.get("/");
    }
    get(id) {
        return http.get(`/${id}`);
    }
    create(data) {

        return http.post("/create",data);
    }

    update(id,data){
        console.log(data);
        return http.patch(`/update/${id}`,data);
    }

    delete(id){
        return http.delete(`/delete/${id}`);
    }
}

export default new TaskDataService();